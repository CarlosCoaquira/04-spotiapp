import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent  { // implements OnInit

  // paises: any[] = [];
  nuevasCanciones: any[] = [];
  loading: boolean;
  error: boolean;
  mensajeError: string;

  constructor(private http: HttpClient,
              private spotify: SpotifyService) {

    // this.http.get('https://restcountries.eu/rest/v2/lang/es')
    // .subscribe( (data: any) => {
    //   this.paises = data;
    //   // console.log(data);
    // });

    // this.spotify.getNewReleases();
    this.error = false;

    this.loading = true;

    this.spotify.getNewReleases()
    .subscribe ( (datos: any) => {
      // console.log(datos.albums.items);
      // console.log(datos);
//      this.nuevasCanciones = datos.albums.items;
        this.nuevasCanciones = datos;

        this.loading = false;
    }, (errorService) => {
      this.error = true;
      this.loading = false;
      console.log(errorService);
      this.mensajeError = errorService.error.error.message;
    } );

   }

  // ngOnInit(): void {
  // }

}
