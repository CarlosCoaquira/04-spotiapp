import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: [
  ]
})
export class ArtistaComponent  { // implements OnInit

  artista: any = {};
  loading: boolean;
  topTracks: any [] = [];

  constructor(private router: ActivatedRoute,
            private spotify: SpotifyService) {

    this.loading = true;
    this.router.params.subscribe( params => {
      // console.log(params);

      this.getArtista(params['id']);
      this.getTopTracks(params['id']);
      // this.loading = false;
    });

  }

  getArtista( id: string){
    this.loading = true;
    this.spotify.getArtista(id).subscribe( dato => {
      // console.log(dato);
      this.artista = dato;
      this.loading = false;
    });
  }

getTopTracks(id: string){
  this.spotify.getArtistaTopTracks(id).subscribe( dato => {
    console.log(dato);
    this.topTracks = dato;
    // this.loading = false;
  });
}

  // ngOnInit(): void {
  // }

}
