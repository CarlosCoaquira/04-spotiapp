import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http: HttpClient) {
    console.log('servicio funcionando');
   }

   private getQuery( query: string){
     const url = `https://api.spotify.com/v1/${ query }`;

      const headers = new HttpHeaders({
        'Authorization': 'Bearer BQDol77SbOogkRjtH2tuQlqmHsDSH0C0m__SPECD2haJVbmPlLOINsPYxG2WSI4thLoe3mebhmJfNZ2kRfI'
      });

      return this.http.get(url, {headers});
   }

   getNewReleases(){

    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQAAjkp70Wj9Gqs1mEQSQQZRwNhi_B9X9L_G-RO_ogcMwtnE3ptHPxO9J-4bsbdRjVMSIA3Rpm_aDeCOj-g'
    // });

    return this.getQuery('browse/new-releases')
            .pipe(map (data => data["albums"].items));

    // retorna el suscribe
    // return this.http.get('https://api.spotify.com/v1/browse/new-releases', {headers})
    //           .pipe( map( data => data["albums"].items )); // return de una sola linea simplificado

    // this.http.get('https://api.spotify.com/v1/browse/new-releases', {headers});
    // .subscribe ( datos => {
    //   console.log(datos);
    // });

   }

   getArtistas(termino: string){
    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQAAjkp70Wj9Gqs1mEQSQQZRwNhi_B9X9L_G-RO_ogcMwtnE3ptHPxO9J-4bsbdRjVMSIA3Rpm_aDeCOj-g'
    // });

    return this.getQuery(`search?q=${termino}&type=artist`)
            .pipe(map (data => data["artists"].items));

    // return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist`, {headers})
    //           .pipe( map( data => data["artists"].items ));  // return de una sola linea simplificado

    // retorna el suscribe
    // return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist`, {headers});

   }

   getArtista( id: string) {
    return this.getQuery(`artists/${id}`);
    // .pipe(map (data => data["artists"].items));
   }

   getArtistaTopTracks( id: string) {
    return this.getQuery(`artists/${id}/top-tracks?country=US`)
    .pipe(map (data => data["tracks"]));
   }
}
